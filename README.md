This is a sample book created with
[BookMD](https://bitbucket.org/elliottslaughter/bookmd), a simple book
authoring tool for Markdkown.

The book itself is *Sense and Sensibility* by Jane Austen. The text
for the book was downloaded from [Project
Gutenberg](http://www.gutenberg.org/) on 2014-04-06, and converted
from its original HTML format to Markdown via Pandoc. BookMD was used
to generate PDF, ePub, and Mobi output from Markdown source. Samples
of the output can be downloaded below:

  * [PDF](https://bitbucket.org/elliottslaughter/bookmd-sample/downloads/latest.pdf) (formatted for 5.5x8.5-inch paper)
  * [ePub](https://bitbucket.org/elliottslaughter/bookmd-sample/downloads/latest.epub)
  * [Mobi](https://bitbucket.org/elliottslaughter/bookmd-sample/downloads/latest.mobi)

The text for the book itself is, of course, public domain. Note that
the Project Gutenberg name is protected by trademark law; see the
license in the book for details. The other supporting files
(`_bookmd.json` and the contents of `etc`, and this README) are also
released into the public domain in the hopes that they will be useful.

The book is organized as follows:

    _bookmd.json

Which contains the configuration for the book (title, author, etc.).

    src/
        00.md
        01.md
        ...

The actual chapters themselves.

    etc/
        templates/
            header.full.latex
            header.part.latex
            template.full.latex
            template.part.latex

Templates to customize the Latex output. I've included only Latex
templates here because I have spent the most time customizing Latex
output, but the principles are similar for other output formats.

    build/

Where the actual output files (PDF, ePub, etc.) are generated.
